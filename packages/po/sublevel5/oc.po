# Occitan (post 1500) translation for debian-installer
# Copyright (c) (c) 2006 Canonical Ltd, and Rosetta Contributors 2006
# This file is distributed under the same license as the debian-installer package.
# Yannig MARCHEGAY <yannick.marchegay@lokanova.com>, 2006.
# Cédric VALMARY <cvalmary@yahoo.fr>, 2016.
# Quentin PAGÈS <quentinantonin@free.fr>, 2020.
#
msgid ""
msgstr ""
"Project-Id-Version: debian-installer sublevel5\n"
"Report-Msgid-Bugs-To: \n"
"POT-Creation-Date: 2024-05-11 20:02+0000\n"
"PO-Revision-Date: 2023-02-11 16:42+0000\n"
"Last-Translator: Quentin PAGÈS <quentinantonin@free.fr>\n"
"Language-Team: Occitan (post 1500) <ubuntu-l10n-oci@lists.ubuntu.com>\n"
"Language: oc\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=2; plural=n > 1;\n"
"X-Generator: Weblate 4.16-dev\n"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid "Use non-free firmware?"
msgstr "Volètz utilizar de micrologicials pas liures ?"

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid ""
"Firmware is a kind of software providing low-level control of certain "
"hardware components (such as Wi-Fi cards or audio chipsets), which may not "
"function fully or at all without it."
msgstr ""

#. Type: boolean
#. Description
#. :sl5:
#: ../apt-mirror-setup.templates:2001
msgid ""
"Although not at all part of Debian, some non-free firmware has been made to "
"work with Debian. This firmware has varying licenses which restrict your "
"freedoms to use, modify, or share the software, and generally does not have "
"source forms that you may study."
msgstr ""

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:58001
#, no-c-format
msgid "ZFS pool %s, volume %s"
msgstr "pool ZFS %s, volum %s"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:60001
#, no-c-format
msgid "DASD %s (%s)"
msgstr "DASD %s (%s)"

#. Type: text
#. Description
#. :sl5:
#: ../partman-base.templates:61001
#, no-c-format
msgid "DASD %s (%s), partition #%s"
msgstr "DASD %s (%s), particion n° %s"

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:60001
msgid ""
"Your boot partition has not been configured with the ext2 file system. This "
"is needed by your machine in order to boot. Please go back and use the ext2 "
"file system."
msgstr ""
"Vòstra particion d'aviada es pas estada configurada amb lo sistèma de "
"fichièr ext2. Aquò's necessari per l'aviada de vòstra maquina. Tornatz en "
"arrièr e utilizatz lo sistèma de fichièr ext2."

#. Type: boolean
#. Description
#. :sl5:
#: ../partman-basicfilesystems.templates:61001
msgid ""
"Your boot partition is not located on the first partition of your hard disk. "
"This is needed by your machine in order to boot.  Please go back and use "
"your first partition as a boot partition."
msgstr ""
"La particion d'aviada es pas dins la primièra particion primària del disc. "
"Aquò's necessari per l'aviada de la maquina. Tornatz en arrièr e utilizatz "
"la primièra particion primària coma particion d'aviada."

#. Type: text
#. Description
#. :sl5:
#. Setting to reserve a small part of the disk for use by BIOS-based bootloaders
#. such as GRUB.
#: ../partman-partitioning.templates:32001
msgid "Reserved BIOS boot area"
msgstr "Zòna d'amorsatge reservada al BIOS"

#. Type: text
#. Description
#. :sl5:
#. short variant of 'Reserved BIOS boot area'
#. Up to 10 character positions
#: ../partman-partitioning.templates:33001
msgid "biosgrub"
msgstr "biosgrub"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "ctc: Channel to Channel (CTC) or ESCON connection"
msgstr "ctc : Canal a Canal (Channel to Channel) o connexion ESCON"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "qeth: OSA-Express in QDIO mode / HiperSockets"
msgstr "qeth : OSA-Express en mòde QDIO / HiperSockets"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "iucv: Inter-User Communication Vehicle - available for VM guests only"
msgstr ""
"iucv : Inter-User Communication Vehicle - sonque disponible pels convidats "
"en VM"

#. Type: select
#. Choices
#. Note to translators : Please keep your translations of the choices
#. below a 65 columns limit (which means 65 characters
#. in single-byte languages) including the initial path
#. :sl5:
#: ../s390-netdevice.templates:1001
msgid "virtio: KVM VirtIO"
msgstr "virtio : KVM VirtIO"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid "Network device type:"
msgstr "Tipe de periferic ret :"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:1002
msgid ""
"Please choose the type of your primary network interface that you will need "
"for installing the Debian system (via NFS or HTTP). Only the listed devices "
"are supported."
msgstr ""
"Mercés de causir lo tipe de l’interfàcia de ret primària que vos farà "
"mestièr per installar lo sistèma Debian (via NFS o HTTP). Solament los "
"periferics listats son preses en carga."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001
msgid "CTC read device:"
msgstr "Crear un periferic multidisc :"

#. Type: select
#. Description
#. :sl5:
#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:2001 ../s390-netdevice.templates:3001
msgid "The following device numbers might belong to CTC or ESCON connections."
msgstr ""
"Los numèro de periferics seguents poirián aperténer al CTC o a las "
"connexions ESCON."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:3001
msgid "CTC write device:"
msgstr "Periferic d’escritura CTC :"

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001 ../s390-netdevice.templates:8001
#: ../s390-netdevice.templates:12001
msgid "Do you accept this configuration?"
msgstr "Acceptatz aquesta configuracion ?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:4001
msgid ""
"The configured parameters are:\n"
" read channel  = ${device_read}\n"
" write channel = ${device_write}\n"
" protocol      = ${protocol}"
msgstr ""
"Los paramètres configurats son :\n"
" canal de lectura  = ${device_read}\n"
" canal d’escritura = ${device_write}\n"
" protocòl          = ${protocol}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "No CTC or ESCON connections"
msgstr "Cap de connexion CTC o ESCON"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:5001
msgid "Please make sure that you have set them up correctly."
msgstr "Asseguratz-vos que los avètz corrèctament configurats."

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:6001
msgid "Protocol for this connection:"
msgstr "Protocòl per aquesta connexion :"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Device:"
msgstr "Periferic :"

#. Type: select
#. Description
#. :sl5:
#: ../s390-netdevice.templates:7001
msgid "Please select the OSA-Express QDIO / HiperSockets device."
msgstr "Mercés de seleccionar lo periferic OSA-Express QDIO / HiperSockets."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:8001
msgid ""
"The configured parameters are:\n"
" channels = ${device0}, ${device1}, ${device2}\n"
" port     = ${port}\n"
" layer2   = ${layer2}"
msgstr ""
"Los paramètres configurats son :\n"
" canals = ${device0}, ${device1}, ${device2}\n"
" pòrt   = ${port}\n"
" layer2 = ${layer2}"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid "No OSA-Express QDIO cards / HiperSockets"
msgstr "Cap d’OSA-Express QDIO cards / HiperSockets"

#. Type: error
#. Description
#. :sl5:
#: ../s390-netdevice.templates:9001
msgid ""
"No OSA-Express QDIO cards / HiperSockets were detected. If you are running "
"VM please make sure that your card is attached to this guest."
msgstr ""
"Cap d’OSA-Express QDIO cards / HiperSockets pas detectada. S’utilizatz una "
"VM mercés de vos assegurar que la carta es ben junta a aqueste convidat."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Port:"
msgstr "Pòrt :"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:10001
msgid "Please enter a relative port for this connection."
msgstr "Picar lo nom d'òste d'aqueste sistèma."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid "Use this device in layer2 mode?"
msgstr "Utilizar aqueste periferic en mòde layer2 ?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:11001
msgid ""
"By default OSA-Express cards use layer3 mode. In that mode LLC headers are "
"removed from incoming IPv4 packets. Using the card in layer2 mode will make "
"it keep the MAC addresses of IPv4 packets."
msgstr ""
"Per defaut las OSA-Express QDIO utilizan lo mòde layer3. Dins aqueste mòde "
"las entèstas LLC son levadas dels paquets IPv4 dintrants. L’utilizacion de "
"la carta en mòde layer2 la farà gardar l’adreça MAC dels paquets IPv4."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-netdevice.templates:12001
msgid ""
"The configured parameter is:\n"
" peer  = ${peer}"
msgstr ""
"Lo paramètre configurat es :\n"
" par  = ${peer}"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "VM peer:"
msgstr "Par VM :"

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid "Please enter the name of the VM peer you want to connect to."
msgstr "Picatz lo nom de la VM en fàcia ont volètz vos connectar."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"If you want to connect to multiple peers, separate the names by colons, e."
"g.  tcpip:linux1."
msgstr ""
"Se volètz vos connectar a mantun pars, separatz los noms amb de dos-punts, "
"ex : tcpip:linux1."

#. Type: string
#. Description
#. :sl5:
#: ../s390-netdevice.templates:13001
msgid ""
"The standard TCP/IP server name on VM is TCPIP; on VIF it's $TCPIP. Note: "
"IUCV must be enabled in the VM user directory for this driver to work and it "
"must be set up on both ends of the communication."
msgstr ""
"Lo nom estandard del servidor TCP/IP sus una VM es TCPIP ; sus VIF es "
"$TCPIP. Nòta : IUCV deu èsser activat sul repertòri utilizaire de la VM per "
"qu’aqueste pilòt foncione e deu èsser configurat suls dos costats de la "
"comunicacion."

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../s390-netdevice.templates:14001
msgid "Configure the network device"
msgstr "Configurar lo periferic ret"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Available devices:"
msgstr "Periferics disponibles :"

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid ""
"The following direct access storage devices (DASD) are available. Please "
"select each device you want to use one at a time."
msgstr ""
"Los periferics dirèctes d’accès a l’emmagazinatge (DASD) seguents son "
"disponibles. Mercés de causir cada periferic que volètz utilizar, un al còp."

#. Type: select
#. Description
#. :sl5:
#: ../s390-dasd.templates:1002
msgid "Select \"Finish\" at the bottom of the list when you are done."
msgstr "Seleccionatz « Acabar » enbàs de la lista quand avètz terminat."

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid "Choose device:"
msgstr "Causir periferic :"

#. Type: string
#. Description
#. :sl5:
#: ../s390-dasd.templates:2001
msgid ""
"Please choose a device. You have to specify the complete device number, "
"including leading zeros."
msgstr ""
"Mercés de causir un periferic. Vos cal especificar lo numèro de periferic "
"complèt, lo zèro de la debuta inclutz."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "Invalid device"
msgstr "Periferic invalid"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:3001
msgid "An invalid device number has been chosen."
msgstr "Nombre de periferics invalid causit."

#. Type: boolean
#. Description
#. :sl5:
#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001 ../s390-dasd.templates:5001
msgid "Format the device?"
msgstr "Formatar lo periferic ?"

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid "The DASD ${device} is already low-level formatted."
msgstr ""
"Aqueste periferic DASD ${device} possedís ja un formatatge de bas nivèl."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:4001
msgid ""
"Please choose whether you want to format again and remove any data on the "
"DASD. Note that formatting a DASD might take a long time."
msgstr ""
"Causissètz se volètz tornar formatar e suprimir tota donada del DASD. Notatz "
"que lo formatatge d’un DASD pòt trigar."

#. Type: boolean
#. Description
#. :sl5:
#: ../s390-dasd.templates:5001
msgid ""
"The DASD ${device} is not low-level formatted. DASD devices must be "
"formatted before you can create partitions."
msgstr ""
"Lo DASD ${device} possedís pas un formatatge de bas nivèl. Los periferics "
"DASD deu èsser formatat abans de crear las particions."

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid "The DASD ${device} is in use"
msgstr "Lo periferic DASD ${device} es utilizat"

#. Type: error
#. Description
#. :sl5:
#: ../s390-dasd.templates:6001
msgid ""
"Could not low-level format the DASD ${device} because the DASD is in use.  "
"For example, the DASD could be a member of a mapped device in an LVM volume "
"group."
msgstr ""
"Formatatge de bas nivèl impossible de DASD ${device} perque lo DASD es "
"utilizat.  Per exemple, lo DASD poiriá pas èsser un membre d’un periferic "
"mapat dins un grop de volum LVM."

#. Type: text
#. Description
#. :sl5:
#: ../s390-dasd.templates:7001
msgid "Formatting ${device}..."
msgstr "Formatatge de ${device}..."

#. Type: text
#. Description
#. Main menu item. Keep translations below 55 columns
#. :sl5:
#: ../s390-dasd.templates:8001
msgid "Configure direct access storage devices (DASD)"
msgstr "Configurar los periferics dirèctes d’accès a l’emmagazinatge (DASD)"

#. Type: text
#. Description
#. Main menu item
#. Translators: keep below 55 columns
#. :sl5:
#: ../zipl-installer.templates:1001
msgid "Install the ZIPL boot loader on a hard disk"
msgstr "Installar lo gestionari d'aviada ZIPL sus un disc dur"
